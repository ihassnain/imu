#include <Wire.h>
#include <Servo.h>
#include <ros.h>
#include<sensor_msgs/Imu.h>
#define SerialPort Serial
#include <SparkFunMPU9250-DMP.h>

#define G 9.80665
#define DEG_TO_RAD 57.2958

MPU9250_DMP imu;
double roll , pitch, yaw;
long int pre_ts = 0;
float qx, qy, qz, qw;


ros::NodeHandle nh;

sensor_msgs::Imu _imu;


ros::Publisher im("imu", &_imu);
void setup()

{

  {
   //SerialPort.begin(115200);

    if (imu.begin() != INV_SUCCESS)
    {
      while (0)
      {
        SerialPort.println("Unable to communicate with MPU-9250");
        SerialPort.println("Check connections, and try again.");
        SerialPort.println();
        delay(3000);
        Serial.println( " Stuck in setup");
        
      }
    }


    imu.setSensors(INV_XYZ_GYRO | INV_XYZ_ACCEL | INV_XYZ_COMPASS);


    imu.setGyroFSR(250); // Set gyro to 2000 dps
    // Accel options are +/- 2, 4, 8, or 16 g
    imu.setAccelFSR(2); // Set accel to +/-2g
    imu.setLPF(10); // Set LPF corner frequency to 5Hz
    imu.setSampleRate(10); // Set sample rate to 10Hz
    imu.setCompassSampleRate(50); // Set mag rate to 10Hz

    nh.initNode();
    nh.advertise(im);
  }

  pre_ts = millis();
}

//}
void loop()
{
  if ( imu.dataReady() )
  {
    imu.update(UPDATE_ACCEL | UPDATE_GYRO | UPDATE_COMPASS);
    printIMUData(millis() - pre_ts);
    pre_ts = millis();
  }
}

void printIMUData(long int dt)
{

  float accelX = imu.calcAccel(imu.ax);
  float accelY = imu.calcAccel(imu.ay);
  float accelZ = imu.calcAccel(imu.az);
  float magX = imu.calcMag(imu.mx);
  float magY = imu.calcMag(imu.my);
  float magZ = imu.calcMag(imu.mz);
  _imu.header.frame_id  =  "imu";
  _imu.linear_acceleration.x = imu.calcAccel(imu.ax) * G;
  _imu.linear_acceleration.y = imu.calcAccel(imu.ay) * G;
  _imu.linear_acceleration.z = imu.calcAccel(imu.az) * G;
  _imu.angular_velocity.x = (imu.calcGyro(imu.gx) / DEG_TO_RAD); 
  _imu.angular_velocity.y = (imu.calcGyro(imu.gy) / DEG_TO_RAD);
  _imu.angular_velocity.z = -(imu.calcGyro(imu.gz) / DEG_TO_RAD);


   Serial.println( " In the loop");
   //Calculation of roll and pitch 
  pitch = atan2 (accelY , ( sqrt ((accelX * accelX) + (accelZ * accelZ))));
  roll = atan2(-accelX , ( sqrt((accelY * accelY) + (accelZ * accelZ))));

  // Calculation of  yaw from mag
  float Yh = (magY * cos(roll)) - (magZ * sin(roll));
  float Xh = (magX * cos(pitch)) + (magY * sin(roll) * sin(pitch)) + (magZ * cos(roll) * sin(pitch));

  yaw =  atan2(Yh, Xh);


  if( yaw >= -3.14 && yaw < 0) {
    yaw = 3.14 + (3.14 + yaw );
  }
  if( pitch >= -3.14 && pitch < 0) {
    pitch = 3.14 + (3.14 + pitch );
  }
  if( roll >= -3.14 && roll < 0) {
    roll = 3.14 + (3.14 + roll );
  }

   // radian conversion to Degree
// roll = roll *  RAD_TO_DEG;
// Serial.println(roll);
//  pitch = pitch *  RAD_TO_DEG;
//   Serial.println(pitch);
//  yaw = yaw *  RAD_TO_DEG;
//   Serial.println( );

   // Quarternion Calculation 
   
  qx = sin(roll / 2) * cos(pitch / 2) * cos(yaw / 2) - cos(roll / 2) * sin(pitch / 2) * sin(yaw / 2);
  qy = cos(roll / 2) * sin(pitch / 2) * cos(yaw / 2) + sin(roll / 2) * cos(pitch / 2) * sin(yaw / 2);
  qz = cos(roll / 2) * cos(pitch / 2) * sin(yaw / 2) - sin(roll / 2) * sin(pitch / 2) * cos(yaw / 2);
  qw = cos(roll / 2) * cos(pitch / 2) * cos(yaw / 2) + sin(roll / 2) * sin(pitch / 2) * sin(yaw / 2);

    // Conversion to ROS IMU message type 
  _imu.orientation.x = qx;
  _imu.orientation.y = qy;
  _imu.orientation.z = qz;
  _imu.orientation.w = qw;

Serial.println( _imu.linear_acceleration.x);


  im.publish(&_imu);

  nh.spinOnce();


}
