#!/usr/bin/env python
import rospy
from sensor_msgs.msg import Imu
from tf.transformations import quaternion_from_euler, euler_from_quaternion
from tf import transformations
import math

def quaternion_to_euler(pose):
    quaternion = [pose.orientation.x, pose.orientation.y,pose.orientation.z, pose.orientation.w]
    euler = transformations.euler_from_quaternion(quaternion)
  #  x = euler[2]*180/math.pi % 360
   # y = euler[2]*180/math.pi % 360
    a = euler[2]*180/math.pi % 360
    return a

def callback(data):
    print(quaternion_to_euler(data))
    
def listener():

    # In ROS, nodes are uniquely named. If two nodes with the same
    # name are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    rospy.init_node('listener', anonymous=True)

    rospy.Subscriber("imu", Imu , callback)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

if __name__ == '__main__':
    listener()
