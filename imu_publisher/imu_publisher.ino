#include <Servo.h>

#include <Wire.h>

#include <ros.h>

#include "sensor_mpu9250.h"
#include "MPU9250.h"
#include<sensor_msgs/Imu.h>

#define G 9.80665
#define DEG_TO_RAD 57.2958

float elapsedTime, currentTime, previousTime;

float gyroAngleX = 0.0;
float gyroAngleY = 0.0;
float gyroAngleZ = 0.0;
float accAngleX= 0.0;
float accAngleY= 0.0;
float roll, pitch, yaw;
float ax,ay,az,gx,gy,gz;
float qx, qy, qz, qw;

//float ang;
ros::NodeHandle nh;

sensor_msgs::Imu _imu;


ros::Publisher imu("imu", &_imu);

void setup() {
  // serial to display data
 Serial.begin(57600);
  mpu9250_setup();
  nh.initNode();
nh.advertise(imu);
}

void time()
{
   previousTime = currentTime;        // Previous time is stored before the actual time read
  currentTime = millis();            // Current time actual time read
  elapsedTime = (currentTime - previousTime) / 1000; // Divide by 1000 to get seconds
}

void loop() {
//  mpu9250_call();
    time();
    IMU.readSensor();



     ax = IMU.getAccelX_mss();
     ay = IMU.getAccelY_mss();
     az = IMU.getAccelZ_mss();
     gx = IMU.getGyroX_rads();
     gy = IMU.getGyroY_rads();
     gz = IMU.getGyroZ_rads();
     
    if(abs(ax) < 2.0){ax = 0;}
    if(abs(ay) < 0.84){ay = 0;}
    if(abs(gx) < 0.013){gx = 0;}
    if(abs(gy) < 0.06){gy = 0;}
    if(abs(gz) < 0.02){gz = 0;}



    accAngleX = (atan(ay/ sqrt(pow(ax, 2) + pow(az, 2)))) ; 
    accAngleY = (atan(-1 * ax/ sqrt(pow(ay, 2) + pow(az, 2)))); 

    gyroAngleX = gyroAngleX + gx* elapsedTime; // deg/s * s = deg
    gyroAngleY = gyroAngleY + gy* elapsedTime;

    yaw =  yaw + gz *elapsedTime;

    roll = 0.96 * gyroAngleX + 0.04 * accAngleX;
    pitch = 0.96 * gyroAngleY + 0.04 * accAngleY;

//    /doc["roll"] = abs(int(roll* 57.29) % 360);
//    /doc["pitch"] = abs(int(pitch* 57.29) % 360);
//    /doc["yaw"] = abs(int(yaw* 57.29) % 360);

    // Quarternion Calculation 
   
    qx = sin(roll / 2) * cos(pitch / 2) * cos(yaw / 2) - cos(roll / 2) * sin(pitch / 2) * sin(yaw / 2);
    qy = cos(roll / 2) * sin(pitch / 2) * cos(yaw / 2) + sin(roll / 2) * cos(pitch / 2) * sin(yaw / 2);
    qz = cos(roll / 2) * cos(pitch / 2) * sin(yaw / 2) - sin(roll / 2) * sin(pitch / 2) * cos(yaw / 2);
    qw = cos(roll / 2) * cos(pitch / 2) * cos(yaw / 2) + sin(roll / 2) * sin(pitch / 2) * sin(yaw / 2);

    // Conversion to ROS IMU message type 
    _imu.header.frame_id  =  "imu";
    _imu.header.stamp = nh.now();

    
    _imu.orientation.x = qx;
    _imu.orientation.y = qy;
    _imu.orientation.z = qz;
    _imu.orientation.w = qw;

    _imu.linear_acceleration.x = ax;
    _imu.linear_acceleration.y = ay;
    _imu.linear_acceleration.z = az;
    _imu.angular_velocity.x = gx; 
    _imu.angular_velocity.y = gy;
    _imu.angular_velocity.z = gz;

    

//_imu.header.frame_id  =  "imu";
//    _imu.linear_acceleration.x = IMU.getAccelX_mss();
//    _imu.linear_acceleration.y = IMU.getAccelY_mss();
//    _imu.linear_acceleration.z = IMU.getAccelZ_mss();
//    _imu.angular_velocity.x = IMU.getGyroX_rads();
//    _imu.angular_velocity.y = IMU.getGyroY_rads();
//    _imu.angular_velocity.z = IMU.getGyroZ_rads();
   // _imu.orientation.w = update_compass();
  
Serial.println(_imu.linear_acceleration.x);
Serial.println("Space");
Serial.println(_imu.angular_velocity.z);
     imu.publish(&_imu);

     nh.spinOnce();
}
