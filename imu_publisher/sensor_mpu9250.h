#include <MPU9250.h>
#include "MPU9250.h"

MPU9250 IMU(Wire, 0x68);
int mpu_status;


void clibrate_acc() {
    mpu_status = IMU.calibrateAccel();
    if (mpu_status) {
        float axb = IMU.getAccelBiasX_mss();
        float axs = IMU.getAccelScaleFactorX();
        float ayb = IMU.getAccelBiasY_mss();
        float ays = IMU.getAccelScaleFactorY();
        float azb = IMU.getAccelBiasZ_mss();
        float azs = IMU.getAccelScaleFactorZ();
        IMU.setAccelCalX(axb, axs);
        IMU.setAccelCalY(ayb, ays);
        IMU.setAccelCalZ(azb, azs);
    }

}

void clibrate_gyro() {

    mpu_status = IMU.calibrateGyro();
    if (mpu_status) {
        float gxb = IMU.getGyroBiasX_rads();
        float gyb = IMU.getGyroBiasY_rads();
        float gzb = IMU.getGyroBiasZ_rads();
        IMU.setGyroBiasX_rads(gxb);
        IMU.setGyroBiasY_rads(gyb);
        IMU.setGyroBiasZ_rads(gzb);

    }

}

void mpu9250_setup(){


    // start communication with IMU
    mpu_status = IMU.begin();
    if (mpu_status < 0) {
        while (0) {}
    }


    // setting the accelerometer full scale range to +/-8G
    IMU.setAccelRange(MPU9250::ACCEL_RANGE_8G);
    // setting the gyroscope full scale range to +/-500 deg/s
    IMU.setGyroRange(MPU9250::GYRO_RANGE_500DPS);
    // setting DLPF bandwidth to 20 Hz
    IMU.setDlpfBandwidth(MPU9250::DLPF_BANDWIDTH_20HZ);
    // setting SRD to 19 for a 50 Hz update rate
    IMU.setSrd(19);
    //calibrate_compass();
    clibrate_acc();
    clibrate_gyro();
}

void mpu9250_call(){
    // read the sensor
    IMU.readSensor();

//    hx = IMU.getMagX_uT();
//    hy = IMU.getMagY_uT();
//    hz = IMU.getMagZ_uT();
//    
//    ax_ = IMU.getAccelX_mss();
//    ay_ = IMU.getAccelY_mss();
//    az_ = IMU.getAccelZ_mss();
//    
//    gx_ = IMU.getGyroX_rads();
//    gy_ = IMU.getGyroY_rads();
//    gz_ = IMU.getGyroZ_rads();
//
//    pitch = atan2(ay_,sqrt((ax_*ax_) + (az_* az_)));
//    roll = atan2(ax_,sqrt((ay_*ay_) + (az_* az_)));
//
//    float yh = (hy *cos(roll))-(hz * sin(roll));
//    float xh = (hx *cos(pitch)) + (hy*sin(roll)*sin(pitch)) + (hz*cos(roll)*sin(pitch));
//
//    yaw = atan2(yh,xh);
    
}
